'use strict';
require('dotenv').config();
const assert = require('assert');
const redis = require('redis');
const redisLegacy = require('redis-legacy'); // Client V3.0.2
const runsv = require('runsv');

const REDIS = process.env.REDIS;
assert(REDIS, 'REDIS env var is required');

const createService = require('..');

// timers/promises is not supported in node < v16
function immediate(result){
	return new Promise((resolve) => {
		resolve(result);
	});
}

function createMock(){
	let mock = {
		calls: [],
		createClient(options){
			mock.calls.push(['createClient', options]);
			return {
				async connect(){
					mock.calls.push(['connect']);
					return await immediate('ok');
				},
				async quit(){
					mock.calls.push(['quit']);
					return await immediate('ok');
				},
				async set(key, value){
					mock.calls.push(['set', key, value]);
					return await immediate(value);
				},
				once(event, callback){
					setImmediate(() => callback());
				}
			};
		}
	};
	return mock;
}
describe('Service',function(){
	describe('Happy path', function(){
		before(async function(){
			this.mock = createMock();
			const service = createService(this.mock, REDIS);
			this.service = service;
			await service.start();
			const client = service.getClient();
			await client.set('k', 1);
			await service.stop();
		});
		it('#start() should connect to redis with provided options', function(){
			const {mock} = this;
			const [createClient, [connect]] = mock.calls;
			assert.deepStrictEqual(createClient[0], 'createClient');
			assert.deepStrictEqual(createClient[1], REDIS);
			assert.deepStrictEqual(connect, 'connect');
		});
		it('#getClient() should return the client', function(){
			const {mock} = this;
			const [,,set] = mock.calls;
			assert.deepStrictEqual(set[0], 'set');
		});
		it('#stop() should quit and remove the client', async function(){
			const {service, mock} = this;
			const [,,, [quit]] = mock.calls;
			assert.deepStrictEqual(quit, 'quit', 'quit not called');
			// Starting the service again will fail if client was not set to null
			await service.start();
		});
	});
	describe('Prevent double start', function(){
		it('should throw an exception if started more than once', async function(){
			const mock = createMock();
			const service = createService(mock, REDIS);
			// First time should work
			await service.start();
			try{
			// second time should fail
				await service.start();
			}catch(e){
				assert.deepStrictEqual(e.message, 'already started: redis');
				return;
			}
			throw new Error('double start did not throw an error');
		});
	});
	describe('Current redis (>= v4) client integration',function(){
		it('should work with current redis', async function(){
			const sv = runsv.create().async();
			sv.addService(createService(redis, {url:REDIS}));
			await sv.start();
			
			const {redis:client} = sv.getClients();
			await client.set('k', 1);
			await sv.stop();
		});
	});
	describe('Legacy redis (v3) client integration',function(){
		it('should work with current redis', function(done){
			const sv = runsv.create();
			sv.addService(createService.v3(redisLegacy, REDIS));
			sv.start(function(err){
				if(err){
					return done(err);
				}
				const {redis:client} = sv.getClients();
				client.set('k', 1);
				sv.stop(done);
			});
		});
	});
});
