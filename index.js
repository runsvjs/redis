'use strict';
const _name = 'redis';
const assert = require('assert');
const {createAsyncService, createCallbackService} = require('runsv-gen-service');

function waitForReadyEvent(client){
	return  new Promise(function(resolve) {
		client.once('ready', function(){
			resolve();
		});
	});
}

let create = function create(redis, options, name=_name){
	assert(redis, 'redis module is required');
	return createAsyncService({
		name, 
		driver:redis,
		async start(deps, redis, options){
			let client = redis.createClient(options);
			const forReadyEvent = waitForReadyEvent(client);
			await client.connect();
			await forReadyEvent;
			return client;
		}, async stop(client){
			await client.quit();
		}
	})(options);
};
create.v3 = function(redis, options, name=_name){
	assert(redis, 'redis module is required');
	return createCallbackService({
		name,
		driver: redis,
		start(deps, redis, options, callback){
			let client = redis.createClient(options);
			client.once('ready', () => callback(null, client));
		},
		stop(client, callback){
			client.quit(callback);
		}
	})(options);
};
module.exports = create;

