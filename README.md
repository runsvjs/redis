[![pipeline status](https://gitlab.com/runsvjs/redis/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/redis/commits/master)
[![coverage report](https://gitlab.com/runsvjs/redis/badges/master/coverage.svg)](https://gitlab.com/runsvjs/redis/commits/master)
# runsv Redis service 

This is a service wrapper around the excellent [node-redis](https://github.com/NodeRedis/node-redis) for [runsv](https://www.github.com/runsvjs/runsv).

## Install

`$ npm install runsv-redis`

## Usage with node-redis >= v4
```javascript
const nodeRedis = require('redis');
const runsv = require('runsv').create().async();
const nodeRedisOptions = {
    url:'...'
};
const redis = require('runsv-redis')(nodeRedis, nodeRedisOptions);

runsv.addService(redis);

await runsv.start();

const redisClient = runsv.getClients().redis;

await	redisClient.hmset(["key", "foo", "bar"]);
```

## Configure node-redis

You can configure your connection to Redis the [same way you](https://github.com/NodeRedis/node-redis#rediscreateclient) you do it with `node-redis`. 


## Usage with node-redis v3
```javascript
const nodeRedis = require('redis');
const runsv = require('runsv').create();

const redis = require('runsv-redis').v3(nodeRedis, 'redis://localhost:6379');

runsv.addService(redis);

runsv.start(function(err){
    const redisClient = runsv.getClients().redis;
    redisClient.hmset(["key", "foo", "bar"]);
});
```
